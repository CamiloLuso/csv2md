#!/bin/sh

# Configuration variables
set -eu

# Command variables
readonly COMMAND="csv2md"

# Functions
csv2md () {
	# Command
	for i in "${@}"
	do
		sed 's/,/ | /g' < "${i}" \
		| awk -v RS='\r\n|\n' '{print "| "$0" |"}'
	done
}

check_files_exist () {
	# No arguments -> stdin
	if [ "${#}" -eq 0 ]
	then
		echo "/dev/stdin"
		return 0
	fi
	
	# Arguments
	return_code=0
	for i in "${@}"
	do
		if [ ! -e "${i}" ]
		then
			echo "${COMMAND}> [ERROR]: file '${i}' does not exist" >&2
			return_code=1
		fi
	done
	return "${return_code}"
}

# Arguments
check_files_exist "${@}"

# Command
csv2md "${@}"
